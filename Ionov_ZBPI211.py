def fact(x):
    if x == 0:
        return 1
    return fact(x-1) * x
    pass


def filter_even(li):
    even = filter(lambda x: x%2 == 0, li)
    return list(even)
    pass


def square(li):
    answer = list(map(lambda x: x**2, li))
    return answer
    pass


def bin_search(li, element):
    for i in li:
        if i == element:
            return li.index(element)
        else:
            continue
    return -1   
    pass


def is_palindrome(string):
    punc = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    # Убираем знаки
    og_string = ''.join(filter(lambda x: not x.isdigit(), string))
    # Убираем пробелы
    og_string = og_string.replace(" ", "")
    # Убираем спец. символы
    for i in og_string:
        if i in punc:
            og_string = og_string.replace(i, "")
    og_string = og_string.lower()

    # Переворачиваем строку
    back_string = ''
    index = len(og_string)
    while index > 0:
        back_string += og_string[index - 1]
        index -= 1

    # Производим проверку
    if og_string == back_string:
        answer = 'YES'
    else:
        answer = 'NO'
    print(answer)    
    return answer
    pass


def calculate(path2file):
    answer = []
    file = open(path2file, mode='r')
    lines_str = file.readlines()
    file.close()
    lines_num = []
    for i in lines_str:
        line = i.split('    ')
        line[0], line[1] = line[1], line[0]
        line = ' '.join(line)
        lines_num.append(line)
    for i in lines_num:
        answer.append(eval(i))
    return print(*answer, sep=',')
    pass


def substring_slice(path2file_1, path2file_2):
    file = open(path2file_1, mode='r')
    lines1 = file.readlines()
    file.close()
    file = open(path2file_2, mode='r')
    lines2 = file.readlines()
    file.close()
    answer = ''
    size = len(lines1)

    for count, i in enumerate(lines2):
        line = i.split(' ')
        line = [int(x) for x in line]
        line[1] = line[1] + 1
        if size != count:
            answer += lines1[count][line[0]:line[1]] + ' '
        else:
            answer += lines1[count][line[0]:line[1]]
    print(answer)
    return answer
    pass


def decode_ch(sting_of_elements):
    import json
    pos = [i for i, e in enumerate(sting_of_elements) if e.isupper()]
    names = []
    for count, start in enumerate(pos):
        if count == len(pos) - 1:
            names.append(sting_of_elements[start:])
        else:
            names.append(sting_of_elements[start:pos[count + 1]])

    periodic_table = json.load(open('periodic_table.json', encoding='utf-8'))

    answer = ''
    for i in names:
        answer += periodic_table[i]
    return answer
    pass


class Student:
    def __init__(self, n='name', s='surname', g=[3, 4, 5]):
        self.name = n
        self.surname = s
        self.fullname = n + ' ' + s
        self.grades = g

    def greeting(self):
        return 'Hello, I am Student'

    def mean_grade(self):
        return sum(self.grades) / len(self.grades)

    def is_otlichnik(self):
        if self.mean_grade() >= 4.5:
            return 'YES'
        else:
            return 'NO'

    def __add__(self, otherStudent):
        return self.name + ' is friends with ' + otherStudent.name

    def __str__(self):
        return self.fullname

    pass


class MyError(Exception):
    def __init__(self, msg):
        self.msg = msg
